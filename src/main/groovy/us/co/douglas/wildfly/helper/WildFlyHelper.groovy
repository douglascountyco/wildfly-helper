package us.co.douglas.wildfly.helper

import org.jboss.as.cli.scriptsupport.CLI


/**
 * Created by cstanton on 2/17/16.
 */
class WildFlyHelper {
    private CLI cli;

    private WildFlyHelper(String host, Integer cliPort, String cliUsername, String cliPassword) {
        cli = newCliInstance(host, cliPort, cliUsername, cliPassword)
    }

    /**
     *
     * @param wildflyConf Any object (e.g. a map) on which the following properties can be found:
     *        <ul>
     *            <li>host (the resolvable machine name or IP)</li>
     *            <li>cliPort (the port on which the CLI interface is listening)</li>
     *            <li>cliUsername (some registered WildFly user with admin rights)</li>
     *            <li>cliPassword (said user's password)</li>
     *        </ul>
     * @param closure A closure to which a WildFlyHelper instance will be passed.  The Helper will
     *                automatically connect before invoking the closure and automatically disconnect
     *                after invoking the closure.
     */
    static void session(wildflyConf,
                        closure) {
        session((String) wildflyConf.host, (Integer) wildflyConf.cliPort,
                (String) wildflyConf.cliUsername, (String) wildflyConf.cliPassword,
                closure)
    }

    static void session(String host,
                        Integer cliPort,
                        String cliUsername,
                        String cliPassword,
                        closure) {
        def helper = new WildFlyHelper(host, cliPort, cliUsername, cliPassword)
        closure(helper)
        helper.disconnect()
    }

    /**
     *
     * @param wildflyConf Any object (e.g. a map) on which the following properties can be found:
     *        <ul>
     *            <li>host (the resolvable machine name or IP)</li>
     *            <li>cliPort (the port on which the CLI interface is listening)</li>
     *            <li>cliUsername (some registered WildFly user with admin rights)</li>
     *            <li>cliPassword (said user's password)</li>
     *        </ul>
     * @return A newly created but not yet connected CLI instance
     */
    static CLI newCliInstance(Map wildflyConf) {
        return newCliInstance((String)wildflyConf.host, (Integer)wildflyConf.cliPort,
                              (String)wildflyConf.cliUsername, (String)wildflyConf.cliPassword)
    }

    static CLI newCliInstance(String host, Integer cliPort,
                              String cliUsername, String cliPassword) {
        CLI cli = CLI.newInstance()
        cli.connect(host, cliPort, cliUsername, cliPassword.toCharArray())
        println ''
        println "WildFlyHelper connected to host: $cli.commandContext.controllerHost:$cli.commandContext.controllerPort"
        return cli
    }

    protected void disconnect() {
        cli.disconnect();
        println ''
        println 'WildFlyHelper disconnected from host'
    }

    static void printCliResult(CLI.Result result) {
        println ''
        println result.getCliCommand()
        println ''
        println result.getResponse()
    }

    CLI getCli() {
        return cli
    }

    WildFlyHelper forceDeploy(File deployable) {
        printCliResult cli.cmd('deploy --force ' + deployable)
        return this
    }

    WildFlyHelper addSqlServerDatasource(ds, db, closure = null) {
        def Map properties = [
                'jndi-name': ds.jndiName,
                'driver-name': ds.driverName,
                'connection-url': "\"jdbc:sqlserver://${db.host}:${db.port};DatabaseName=${db.name}\"",
                'user-name': db.username,
                'password': db.password,
                'valid-connection-checker-class-name': 'org.jboss.jca.adapters.jdbc.extensions.mssql.MSSQLValidConnectionChecker',
                'enabled': true
        ]
        if (closure) closure(properties)
        def cliCommand = "/subsystem=datasources/data-source=${ds.name}:add(${properties.iterator().join(",")})"
        printCliResult cli.cmd(cliCommand)
        return this
    }

    WildFlyHelper addProperty(String key, String value) {
        printCliResult cli.cmd("/system-property=$key:add(value=\"$value\")")
        return this
    }

    WildFlyHelper undeploy(String artifactName) {
        printCliResult cli.cmd("undeploy $artifactName")
        return this
    }

    WildFlyHelper removeDatasource(String dsName) {
        printCliResult cli.cmd("data-source remove --name=$dsName")
        return this
    }

    WildFlyHelper removeProperty(String key) {
        printCliResult cli.cmd("/system-property=$key:remove")
        return this
    }

    WildFlyHelper reload() {
        printCliResult cli.cmd('reload')
        return this
    }
}
